#include "st7735.h"

int st_init(st7735_t *st, SPI_HandleTypeDef *spi, st_pin_t DCpin, st_pin_t CSpin) {
	st->st_dc = DCpin;
	st->st_cs = CSpin;

	GPIO_InitTypeDef gpio_init;

	gpio_init.Pin = DCpin.GPIO_Pin;
	gpio_init.Mode = GPIO_MODE_OUTPUT_PP;
	gpio_init.Pull = GPIO_PULLDOWN;
	gpio_init.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(DCpin.GPIOx, &gpio_init);
	gpio_init.Pin = CSpin.GPIO_Pin;
	HAL_GPIO_Init(CSpin.GPIOx, &gpio_init);

	HAL_GPIO_WritePin(st->st_cs.GPIOx, st->st_cs.GPIO_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(st->st_dc.GPIOx, st->st_dc.GPIO_Pin, GPIO_PIN_RESET);

	st->spi_dev = spi;
	st->width = 128;
	st->height = 160;

	// INIT
	st_cmd(st, SWRESET);
	HAL_Delay(200);
	st_cmd(st, SLPOUT);
	HAL_Delay(200);
	st_cmd(st, MADCTL);
	st_write_byte(st, MAD_RGB, 1);
	st_cmd(st, COLMOD);
	st_write_byte(st, COLOR_18BIT, 1);
	st_cmd(st, DISPON);
	HAL_Delay(200);
	return 0;
}

void st_write_byte(st7735_t *st, uint8_t b, GPIO_PinState dc) {
	HAL_GPIO_WritePin(st->st_dc.GPIOx, st->st_dc.GPIO_Pin, dc);
	HAL_GPIO_WritePin(st->st_cs.GPIOx, st->st_cs.GPIO_Pin, GPIO_PIN_RESET);
	while (HAL_SPI_Transmit(st->spi_dev, &b, sizeof(uint8_t), HAL_MAX_DELAY) != HAL_OK);
	while(HAL_SPI_GetState(st->spi_dev) != HAL_SPI_STATE_READY);
	HAL_GPIO_WritePin(st->st_cs.GPIOx, st->st_cs.GPIO_Pin, GPIO_PIN_SET);
}

void st_write_word(st7735_t *st, uint16_t w) {
	st_write_byte(st, w >> 8, GPIO_PIN_SET);
	st_write_byte(st, w & 0xFF, GPIO_PIN_SET);
}

void st_cmd(st7735_t *st, uint8_t cmd) {
	st_write_byte(st, cmd, GPIO_PIN_RESET);
}

void st_window(st7735_t *st, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1) {
	st_cmd(st, CASET);
	st_write_word(st, x0);
	st_write_word(st, x1);
	st_cmd(st, RASET);
	st_write_word(st, y0);
	st_write_word(st, y1);
}

static void _send_pixel(st7735_t *st, rgb_color_t color) {
	HAL_GPIO_WritePin(st->st_dc.GPIOx, st->st_dc.GPIO_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(st->st_cs.GPIOx, st->st_cs.GPIO_Pin, GPIO_PIN_RESET);
	while (HAL_SPI_Transmit(st->spi_dev, color.rgb, sizeof(color.rgb), HAL_MAX_DELAY) != HAL_OK);
	while(HAL_SPI_GetState(st->spi_dev) != HAL_SPI_STATE_READY);
	HAL_GPIO_WritePin(st->st_cs.GPIOx, st->st_cs.GPIO_Pin, GPIO_PIN_SET);
}

void st_draw_pixel(st7735_t *st, uint16_t x, uint16_t y, rgb_color_t color) {
	if (x < st->width && y < st->height) {
		st_window(st, x, y, x, y);
		st_cmd(st, RAMWR);
		_send_pixel(st, color);
	}
}

void st_fill_rect(st7735_t *st, uint16_t x, uint16_t y, uint16_t w, uint16_t h, rgb_color_t color) {
	st_window(st, x, y, x + w - 1, y + h - 1);
	st_cmd(st, RAMWR);
	size_t i = 0;
	for (; i < h; ++i) {
		size_t j = 0;
		for (; j < w; ++j) {
			_send_pixel(st, color);
		}
	}
}

void st_clear(st7735_t *st, rgb_color_t color) {
	st_fill_rect(st, 0, 0, st->width, st->height, color);
}

void st_inversion(st7735_t *st, uint8_t on) {
	if (on) {
		st_cmd(st, INVON);
	} else {
		st_cmd(st, INVOFF);
	}
}

void st_put_char(st7735_t *st, char c, uint16_t x, uint16_t y, rgb_color_t fgc, rgb_color_t bgc) {
	if (c >= 'a' && c <= 'z') {
		c -= 32;
	}
	const uint8_t *g = &(font8x10.glyph[(c - font8x10.start_c) * 10]);
	size_t i = 0;
	uint8_t mask = 1 << (font8x10.width - 1);
	st_window(st, x, y, x + font8x10.width - 1, y + font8x10.height - 1);
	st_cmd(st, RAMWR);
	for (; i < font8x10.height; ++i) {
		size_t j = 0;
		for (; j < font8x10.width; ++j) {
			if ((g[i] << j) & mask) {
				st_draw_pixel(st, x + j, y + i, fgc);
			} else {
				st_draw_pixel(st, x + j, y + i, bgc);
			}
		}
	}
}

void st_put_string(st7735_t *st, const char *str, uint16_t x, uint16_t y, rgb_color_t fgc, rgb_color_t bgc) {
	uint16_t px = x, py = y;
	size_t i = 0;
	for (; str[i]; ++i) {
		if (str[i] == '\n' || (px + font8x10.width + 1) >= st->width) {
			if ((py + font8x10.height + 1) >= st->height)
				break;
			py += font8x10.height + 1;
			px = x;
			if (str[i] == '\n')
				continue;
		}
		if (str[i] != ' ') {
			st_put_char(st, str[i], px, py, fgc, bgc);
		} else {
			st_fill_rect(st, px, py, font8x10.width, font8x10.height, bgc);
		}
		px += font8x10.width + 1;
	}
}

void st_put_int(st7735_t *st, int num, uint16_t x, uint16_t y, rgb_color_t fgc, rgb_color_t bgc) {
	char sn[11] = {0};
	int i = 9;
	while (num) {
		sn[i] = (num % 10) + '0';
		num /= 10;
		--i;
	}
	++i;
	st_put_string(st, sn + i, x, y, fgc, bgc);
}

#define ABS(a) ((a) < 0 ? -(a) : (a))
#define SWAP(a, b) 	\
{					\
	int t = a;		\
	a = b;			\
	b = t;			\
}					\

void st_draw_line(st7735_t *st, uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, rgb_color_t color) {
	int deltax = ABS((int)x1 - (int)x0);
	int deltay = ABS((int)y1 - (int)y0);
	
	if (x0 > x1 || y0 > y1) {
		SWAP(x0, x1)
		SWAP(y0, y1)
	}

	if (deltax >= deltay) {
		int diry = (int)y1 - (int)y0;
		if (diry > 0) {
			diry = 1;
		} else if (diry < 0) {
			diry = -1;
		}
		int err = 0;
		int deltaerr = deltay + 1;
		int y = y0;
		int x = x0;
		for (; x <= x1; ++x) {
			st_draw_pixel(st, x, y, color);
			err += deltaerr;
			if (err >= deltax + 1) {
				y += diry;
				err = err - (deltax + 1);
			}
		}
	} else {
		int dirx = (int)x1 - (int)x0;
		if (dirx > 0) {
			dirx = 1;
		} else if (dirx < 0) {
			dirx = -1;
		}
		int err = 0;
		int deltaerr = deltax + 1;
		int x = x0;
		int y = y0;
		for (; y <= y1; ++y) {
			st_draw_pixel(st, x, y, color);
			err += deltaerr;
			if (err >= deltay + 1) {
				x += dirx;
				err = err - (deltay + 1);
			}
		}
	}
}
