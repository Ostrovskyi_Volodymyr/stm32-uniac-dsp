#include <stdint.h>

typedef struct {
	uint8_t width, height;
	char start_c;
	const uint8_t *glyph;
} st_font_t;

extern st_font_t font8x10;
